package com.remoter.provider.internal.util;

import com.remoter.api.configure.Option;
import com.remoter.api.util.NetUtil;

public class FinalProviderInternal {
	
	public static final Option<String> O_PROVIDER_INTERNAL_TASK_NAME = Option.create("provider.internal.task_name","TASK");
	public static final Option<String> O_PROVIDER_INTERNAL_HOST = Option.create("provider.internal.host",NetUtil.getLocalAddress().getHostAddress());
	public static final Option<Integer> O_PROVIDER_INTERNAL_PORT = Option.create("provider.internal.port",9901);
	
}