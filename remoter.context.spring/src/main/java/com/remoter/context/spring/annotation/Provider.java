package com.remoter.context.spring.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.remoter.api.util.Final;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Provider {

	public Class<?> type();
	public String name();
	
	public int weight() default Final.D_LOADBALANCE_WEIGHT;
	public int executes() default Final.D_EXECUTES;
	
}