package com.remoter.context.spring.util;

import com.remoter.api.configure.Option;

public class FinalContainerInternal {
	
	public static final Option<String> O_CONTAINER_SPRING_CONFIG = Option.create("container.spring.config","classpath*:META-INF/spring/*.xml");
	
}