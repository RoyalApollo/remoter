package com.remoter.context.spring;

import java.util.Set;

import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.beans.factory.xml.XmlReaderContext;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import com.remoter.context.spring.annotation.Provider;

public class RemoterClassPathBeanDefinitionScanner extends ClassPathBeanDefinitionScanner{

	public RemoterClassPathBeanDefinitionScanner(ParserContext parserContext){
		super(parserContext.getRegistry(),false);
		XmlReaderContext readerContext = parserContext.getReaderContext();
		this.setResourceLoader(readerContext.getResourceLoader());
		this.setEnvironment(readerContext.getEnvironment());
		this.setBeanDefinitionDefaults(parserContext.getDelegate().getBeanDefinitionDefaults());
		this.setAutowireCandidatePatterns(parserContext.getDelegate().getAutowireCandidatePatterns());
		this.addIncludeFilter(new AnnotationTypeFilter(Provider.class));
	}
	
	public Set<BeanDefinitionHolder> scanComplete(String... basePackages){
        return doScan(basePackages);
    }
	
}