package com.remoter.context.spring.boot;

import com.remoter.api.configure.IConfiguration;
import com.remoter.api.configure.support.AbstractConfiguration;
import com.remoter.api.context.IContextService;
import com.remoter.api.extension.support.ExtensionLoader;
import com.remoter.api.util.Final;


public class RemoterContextStarter {
	
	public static final String ID = RemoterContextStarter.class.getName();
	
	private final IConfiguration configuration;
	private final IContextService contextService;
	
	public RemoterContextStarter(String config){
		this.configuration = AbstractConfiguration.getConfiguration();
		this.contextService = ExtensionLoader.getService(IContextService.class,this.configuration.getOption(Final.O_SERVER_CONTEXT));
	}
	
	public void start(){
		this.contextService.start();
	}
	
	public void close(){
		this.contextService.close();
	}
	
}