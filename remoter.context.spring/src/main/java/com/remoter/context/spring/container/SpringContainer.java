package com.remoter.context.spring.container;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.remoter.api.configure.IConfiguration;
import com.remoter.api.configure.support.AbstractConfiguration;
import com.remoter.api.container.IContainer;
import com.remoter.api.extension.annotation.ExtensionName;
import com.remoter.context.spring.util.FinalContainerInternal;

@ExtensionName("spring")
public class SpringContainer implements IContainer{
	
	private ClassPathXmlApplicationContext context;
	private IConfiguration configuration;
	
	@Override
	public void start(){
		this.configuration = AbstractConfiguration.getConfiguration();
		this.context = new ClassPathXmlApplicationContext(this.configuration.getOption(FinalContainerInternal.O_CONTAINER_SPRING_CONFIG));
		this.context.start();
	}

	@Override
	public void close(){
		if(null != this.context){
			this.context.close();
		}
	}
	
}