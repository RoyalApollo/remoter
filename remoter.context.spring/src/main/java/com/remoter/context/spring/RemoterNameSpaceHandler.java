package com.remoter.context.spring;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

public class RemoterNameSpaceHandler extends NamespaceHandlerSupport{

	@Override
	public void init(){
		this.registerBeanDefinitionParser("annotation-driven",new RemoterBeanDefinitionParser());
	}
	
}