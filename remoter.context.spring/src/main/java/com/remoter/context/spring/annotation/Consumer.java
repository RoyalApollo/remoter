package com.remoter.context.spring.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.remoter.api.util.Final;

@Target({ElementType.FIELD,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Consumer {
	
	public Class<?> type() default Void.class;
	public String name();
	
	public int timeout() default Final.D_TIMEOUT;
	public String loadBalance() default Final.D_LOADBALANCE_VALUE;
	public boolean async() default Final.D_ASYNC;
	public boolean check() default Final.D_CHECK;
	
}