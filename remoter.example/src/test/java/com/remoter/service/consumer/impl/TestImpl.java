package com.remoter.service.consumer.impl;

import org.springframework.stereotype.Service;

import com.remoter.context.spring.annotation.Consumer;
import com.remoter.service.consumer.ITest;
import com.remoter.service.provider.IMathService;

@Service
public class TestImpl implements ITest{

	@Consumer(name="mathServiceImpl")
	private IMathService mathService;
	
	@Override
	public void testValue() {
		System.out.println(this.mathService.add(10,20));
	}
	
}