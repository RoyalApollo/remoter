package com.remoter.service.provider.impl;

import com.remoter.context.spring.annotation.Provider;
import com.remoter.service.provider.IMathService;

@Provider(type=IMathService.class,name="mathServiceImpl")
public class MathServiceImpl implements IMathService{

	@Override
	public int add(int a, int b) {
		return a + b;
	}
	
}