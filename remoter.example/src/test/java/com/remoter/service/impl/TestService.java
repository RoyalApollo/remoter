package com.remoter.service.impl;

import org.springframework.stereotype.Service;

import com.remoter.context.spring.annotation.Consumer;
import com.remoter.service.IPersonService;

@Service
public class TestService {
	
	@Consumer(type=IPersonService.class,name="teacherServiceImpl")
	private IPersonService personService;
	
	public String say(){
		System.out.println(this.personService);
		this.personService.say("测试");
		return "test";
	}
	
}