package com.remoter.service.impl;

import com.remoter.context.spring.annotation.Provider;
import com.remoter.service.IPersonService;

@Provider(type=IPersonService.class,name="studentServiceImpl")
public class StudentServiceImpl implements IPersonService{

	@Override
	public String say(String name) {
		System.out.println("StudentServiceImpl:" + name);
		return "student:"+name;
	}
	
}