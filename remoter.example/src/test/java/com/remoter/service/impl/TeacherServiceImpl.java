package com.remoter.service.impl;

import com.remoter.context.spring.annotation.Provider;
import com.remoter.service.IPersonService;

@Provider(type=IPersonService.class,name="teacherServiceImpl")
public class TeacherServiceImpl implements IPersonService{

	@Override
	public String say(String name) {
		System.out.println("TeacherServiceImpl:" + name);
		return "teacher:"+name;
	}
	
}