package com.remoter.example.spring;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.remoter.service.consumer.ITest;

public class TestSpringConsumer {
	
	public static void main(String[] args) throws IOException {
		try(ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-consumer.xml")){
			while(true){
				try{
					context.getBean(ITest.class).testValue();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
	
}