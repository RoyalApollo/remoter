package com.remoter.example.spring;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.remoter.service.impl.TestService;

public class TestSpring {
	
	public static void main(String[] args) throws IOException {
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-core.xml");
		
		TestService service = context.getBean(TestService.class);
		System.out.println("-----------------"+service);
		service.say();
		
		System.in.read();
		
		context.close();
		
	}
	
}