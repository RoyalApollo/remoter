package com.remoter.example.spring;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.remoter.api.extension.support.ExtensionLoader;
import com.remoter.api.monitor.IMonitorService;

public class TestSpringProvider {
	
	public static void main(String[] args) throws IOException {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-provider.xml");
		
		IMonitorService monitorService = ExtensionLoader.getService(IMonitorService.class,"internal");
		System.out.println(monitorService.getBind());
		System.out.println(monitorService.getAllExporters());
		
		System.in.read();
		context.close();
	}
	
}