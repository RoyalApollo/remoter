package com.remoter.example.registry;

import java.util.concurrent.locks.LockSupport;

import com.remoter.api.configure.support.AbstractConfiguration;
import com.remoter.api.configure.IConfiguration;
import com.remoter.api.context.support.AbstractModule;
import com.remoter.api.context.support.Exporter;
import com.remoter.api.context.support.Importer;
import com.remoter.api.extension.support.ExtensionLoader;
import com.remoter.api.registry.IRegistry;
import com.remoter.api.registry.IRegistryListener;
import com.remoter.api.util.Final;

public class TestRegistry {
	
	public static void main(String[] args) {
		
		IConfiguration configuration = AbstractConfiguration.getConfiguration();
		IRegistry registry = ExtensionLoader.getService(IRegistry.class,configuration.getOption(Final.O_SERVER_REGISTRY));
		registry.attachListener(new IRegistryListener(){
			@Override
			public void onUpdate(String path,Object data) {
				System.out.println("---------------------------");
				System.out.println("onUpdate");
				System.out.println(path);
				System.out.println(data);
				System.out.println("---------------------------");
			}
			@Override
			public void onDelete(String path, Object data) {
				System.out.println("---------------------------");
				System.out.println("onDelete");
				System.out.println(path);
				System.out.println("---------------------------");
			}
			@Override
			public void onCreate(String path, Object data) {
				System.out.println("---------------------------");
				System.out.println("onCreate");
				System.out.println(path);
				System.out.println(data);
				System.out.println("---------------------------");
			}
		});
		registry.start();
		
		Exporter exporter = AbstractModule.createExporter("os","localhost",123,"aaa",IRegistry.class);
		registry.attachExporter(exporter);
		
		Importer importer = AbstractModule.createImporter("os","localhost",123,"aaa",IRegistry.class);
		registry.attachImporter(importer);
		
		System.out.println("---");
		System.out.println(registry.exporters().get(0).getPath());
		System.out.println(registry.importers().get(0).getPath());
		System.out.println("---");
//		registry.detachExporter(exporter);
		LockSupport.park();
	}
	
}