package com.remoter.example.provider;

import java.io.IOException;
import java.net.BindException;
import java.net.InetSocketAddress;

import com.remoter.api.configure.IConfiguration;
import com.remoter.api.configure.support.AbstractConfiguration;
import com.remoter.api.extension.support.ExtensionLoader;
import com.remoter.api.provider.IProviderService;
import com.remoter.api.util.Final;

public class TestProvider {
	
	public static void main(String[] args) throws IOException {
		
		InetSocketAddress bind = new InetSocketAddress("localhost",9527);
		
		IConfiguration configuration = AbstractConfiguration.getConfiguration();
		IProviderService providerService = ExtensionLoader.getService(IProviderService.class,configuration.getOption(Final.O_SERVER_PROVIDER));
		try {
			providerService.bind(bind);
		} catch (BindException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.in.read();
		providerService.unBind();
		
	}
	
}