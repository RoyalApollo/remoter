package com.remoter.example.config;

import com.remoter.api.configure.IConfiguration;
import com.remoter.api.configure.support.AbstractConfiguration;
import com.remoter.api.util.Final;

public class TestConfig {
	
	public static void main(String[] args) {
		
		IConfiguration configuration = AbstractConfiguration.getConfiguration();
		System.out.println(configuration.getOption(Final.O_SERVER_NAME));
		
	}
	
}