package com.remoter.example.consumer;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.remoter.api.configure.IConfiguration;
import com.remoter.api.configure.support.AbstractConfiguration;
import com.remoter.api.consumer.IConsumerService;
import com.remoter.api.exception.ConnectErrorException;
import com.remoter.api.extension.support.ExtensionLoader;
import com.remoter.api.util.Final;

public class TestConsumer {

	public static void main(String[] args) throws IOException {
		
		InetSocketAddress remote = new InetSocketAddress("localhost",9527);
		
		IConfiguration configuration = AbstractConfiguration.getConfiguration();
		IConsumerService consumerService = ExtensionLoader.getService(IConsumerService.class,configuration.getOption(Final.O_SERVER_CONSUMER));
		
		try {
			consumerService.connect(remote);
		} catch (ConnectErrorException e) {
			e.printStackTrace();
		}

		System.in.read();
		consumerService.destory();
		
	}

}