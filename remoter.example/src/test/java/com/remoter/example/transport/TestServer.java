package com.remoter.example.transport;

import java.net.InetSocketAddress;

import com.remoter.api.util.NetUtil;
import com.remoter.transport.netty.NettyServer;

public class TestServer {
	
	public static void main(String[] args){
		
		InetSocketAddress bind = new InetSocketAddress(NetUtil.getLocalAddress(),9527);
		NettyServer server = new NettyServer();
		try {
			server.bind(bind);
		} catch (Exception e) {
			e.printStackTrace();
			server.disConnect();
		}
		
	}
	
}