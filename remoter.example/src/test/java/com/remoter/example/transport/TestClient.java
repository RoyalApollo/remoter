package com.remoter.example.transport;

import java.net.InetSocketAddress;

import io.netty.channel.nio.NioEventLoopGroup;

import com.remoter.api.exception.ConnectErrorException;
import com.remoter.api.util.NetUtil;
import com.remoter.transport.netty.NettyClient;

public class TestClient {
	
	public static void main(String[] args) {
		NioEventLoopGroup work = new NioEventLoopGroup();
		InetSocketAddress remote = new InetSocketAddress(NetUtil.getLocalAddress(),9527);
		NettyClient client = new NettyClient();
		try {
			client.connect(remote);
		} catch (ConnectErrorException e) {
			e.printStackTrace();
			client.disConnect();
			work.shutdownGracefully();
		}
	}
	
}