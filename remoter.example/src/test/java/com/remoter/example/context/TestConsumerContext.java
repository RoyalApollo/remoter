package com.remoter.example.context;

import java.io.IOException;

import com.remoter.api.configure.IConfiguration;
import com.remoter.api.configure.support.AbstractConfiguration;
import com.remoter.api.context.IContextService;
import com.remoter.api.context.support.Importer;
import com.remoter.api.extension.support.ExtensionLoader;
import com.remoter.api.util.Final;
import com.remoter.service.IPersonService;

public class TestConsumerContext {
	
	public static void main(String[] args) throws IOException {
		IConfiguration configuration = AbstractConfiguration.getConfiguration();
		IContextService context = ExtensionLoader.getService(IContextService.class,configuration.getOption(Final.O_SERVER_CONTEXT));
		
		context.start();
		
		Importer t = context.createImporter("teacher",IPersonService.class);
		context.attachImporter(t);
		
		while(true){
			try{
				System.out.println("data:"+t.getProxy(IPersonService.class).say("测试"));
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
//		System.in.read();
//		context.close();
	}
	
}