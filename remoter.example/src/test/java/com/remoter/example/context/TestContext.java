package com.remoter.example.context;

import java.io.IOException;

import com.remoter.api.configure.IConfiguration;
import com.remoter.api.configure.support.AbstractConfiguration;
import com.remoter.api.context.IContextService;
import com.remoter.api.context.support.Exporter;
import com.remoter.api.context.support.Importer;
import com.remoter.api.extension.support.ExtensionLoader;
import com.remoter.api.util.Final;
import com.remoter.service.IPersonService;
import com.remoter.service.impl.TeacherServiceImpl;

public class TestContext {
	
	public static void main(String[] args) throws IOException {
		
		IConfiguration configuration = AbstractConfiguration.getConfiguration();
		IContextService context = ExtensionLoader.getService(IContextService.class,configuration.getOption(Final.O_SERVER_CONTEXT));
		
		context.start();
		
		Exporter teacher = context.createExporter("teacher",IPersonService.class,new TeacherServiceImpl());
		//Exporter student = context.createExporter("student",IPersonService.class,new StudentServiceImpl());
		
		context.attachExporter(teacher);
		//context.attachExporter(student);
		
		System.out.println(context);
		
		Importer t = context.createImporter("teacher",IPersonService.class);
		//Importer s = context.createImporter("student",IPersonService.class);
		context.attachImporter(t);
		//context.attachImporter(s);
		
		System.out.println("data:"+t.getProxy(IPersonService.class).say("测试"));
//		System.out.println("data:"+s.getProxy(IPersonService.class).say("测试"));
		
		System.in.read();
		context.close();
		
	}
	
}