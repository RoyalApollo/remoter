package com.remoter.context.internal;

import com.remoter.api.context.support.AbstractContextService;
import com.remoter.api.extension.annotation.ExtensionName;
import com.remoter.context.internal.util.FinalContextInternal;

@ExtensionName("internal")
public class InternalContextService extends AbstractContextService{
	
	@Override
	protected void onStart() {
		
	}

	@Override
	protected void onClose() {
	}

	@Override
	public String getDefaultLoadBalance(){
		return this.configuration.getOption(FinalContextInternal.O_CONTEXT_LOAD_BALANCE);
	}
	
}