package com.remoter.api;

import com.remoter.api.extension.support.ExtensionLoader;
import com.remoter.biz.IPerson;

public class TestExtension {
	
	public static void main(String[] args) {
		
		System.out.println(ExtensionLoader.getService(IPerson.class).hello("测试"));
		System.out.println(ExtensionLoader.getService(IPerson.class,"teacher").hello("测试"));
		
		System.out.println(ExtensionLoader.getService(IPerson.class));
		System.out.println(ExtensionLoader.getService(IPerson.class));
		
		System.out.println(ExtensionLoader.getAdaptiveService(IPerson.class));
		System.out.println(ExtensionLoader.getAdaptiveService(IPerson.class));
		
	}
	
}