package com.remoter.biz;

import com.remoter.api.extension.annotation.Extension;
import com.remoter.api.extension.annotation.ExtensionAdaptive;
import com.remoter.api.extension.annotation.ExtensionAdaptiveParam;
import com.remoter.api.extension.support.ExtensionScope;

@Extension(value="student",scope=ExtensionScope.Prototype)
public interface IPerson {
	
	public String hello(String name);
	
	@ExtensionAdaptive
	public String say(@ExtensionAdaptiveParam String name);
	
}