package com.remoter.biz.impl;

import com.remoter.biz.IPerson;

public class StudentBiz implements IPerson{

	@Override
	public String hello(String name) {
		return "同学你好:" + name;
	}

	@Override
	public String say(String name) {
		return "student:" + name;
	}
	
}