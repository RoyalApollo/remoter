package com.remoter.biz.impl;

import com.remoter.api.extension.annotation.ExtensionName;
import com.remoter.biz.IPerson;

@ExtensionName("teacher")
public class TeacherBiz implements IPerson{

	@Override
	public String hello(String name) {
		return "老师你好:" + name;
	}
	
	@Override
	public String say(String name) {
		return "teacher:" + name;
	}
	
}