package com.remoter.api.container;

import com.remoter.api.extension.annotation.Extension;

@Extension
public interface IContainer {
	
	public void start();
	
	public void close();
	
}