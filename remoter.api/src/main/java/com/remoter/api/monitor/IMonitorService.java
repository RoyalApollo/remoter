package com.remoter.api.monitor;

import java.net.InetSocketAddress;
import java.util.List;

import com.remoter.api.consumer.IConsumerService;
import com.remoter.api.context.IContextService;
import com.remoter.api.extension.annotation.Extension;
import com.remoter.api.provider.IProviderService;

@Extension
public interface IMonitorService {
	
	public void start(IContextService contextService,IProviderService providerService,IConsumerService consumerService);
	
	public void close();
	
	/**
	 * 返回当前服务器所绑定的ip及端口
	 * @return
	 */
	public InetSocketAddress getBind();
	
	/**
	 * 返回当前服务器对应的ip及端口所提供的服务
	 * @param address
	 * @return
	 */
	public List<String> getAllExporters();
	
}