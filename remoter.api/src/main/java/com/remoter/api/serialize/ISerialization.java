package com.remoter.api.serialize;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.remoter.api.extension.annotation.Extension;

/**
 * @author			koko
 * @date 			2017-06-29 18:43:55
 * @description 	类功能说明
 */
@Extension
public interface ISerialization {
	
	public void serialize(Object data,OutputStream outputStream)throws IOException;
	
	public Object deserialize(InputStream inputStream)throws IOException;
	
}