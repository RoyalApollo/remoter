package com.remoter.api.consumer;

import java.net.InetSocketAddress;

import com.remoter.api.exception.ConnectErrorException;
import com.remoter.api.extension.annotation.Extension;
import com.remoter.api.transport.IClient;

@Extension
public interface IConsumerService {
	
	public IClient connect(InetSocketAddress remote) throws ConnectErrorException;
	
	public boolean hasClient(InetSocketAddress remote);
	
	public IClient getClient(InetSocketAddress remote,boolean autoCreate);
	
	public void disConnect(InetSocketAddress remote);
	
	public void destory();
	
	public InetSocketAddress getBindSocketAddress();
	
}