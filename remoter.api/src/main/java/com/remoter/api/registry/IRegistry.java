package com.remoter.api.registry;

import java.util.List;

import com.remoter.api.context.support.Exporter;
import com.remoter.api.context.support.Importer;
import com.remoter.api.extension.annotation.Extension;

@Extension
public interface IRegistry {
	
	public void start();
	public void close();
	
	public boolean attachExporter(Exporter exporter);
	public boolean attachImporter(Importer importer);
	
	public boolean detachExporter(Exporter exporter);
	public boolean detachImporter(Importer importer);
	
	public boolean attachListener(IRegistryListener registryListener);
	public boolean detachListener(IRegistryListener registryListener);
	
	public List<Exporter> exporters();
	public List<Importer> importers();
	
}