package com.remoter.api.registry;

public interface IRegistryListener {
	
	public void onCreate(String path,Object data);
	public void onUpdate(String path,Object data);
	public void onDelete(String path,Object data);
	
}