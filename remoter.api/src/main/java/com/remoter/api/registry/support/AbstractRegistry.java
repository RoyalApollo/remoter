package com.remoter.api.registry.support;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.remoter.api.registry.IRegistry;
import com.remoter.api.registry.IRegistryListener;

public abstract class AbstractRegistry implements IRegistry{
	
	protected static final Logger logger = LoggerFactory.getLogger(AbstractRegistry.class);
	
	protected List<IRegistryListener> listeners = new ArrayList<IRegistryListener>();
	
	@Override
	public boolean attachListener(IRegistryListener registryListener) {
		if(this.listeners.contains(registryListener)){
			return false;
		}
		this.listeners.add(registryListener);
		return true;
	}

	@Override
	public boolean detachListener(IRegistryListener registryListener) {
		if(this.listeners.contains(registryListener)){
			this.listeners.remove(registryListener);
			return true;
		}
		return false;
	}
	
}