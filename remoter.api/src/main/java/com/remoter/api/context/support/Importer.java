package com.remoter.api.context.support;

import com.remoter.api.util.Final;


public final class Importer extends AbstractModule{

	private static final long serialVersionUID = -3681225674260135151L;
	
	protected Importer(String server, String host, int port, String bean,Class<?> type) {
		super(server, host, port, bean, type);
	}
	
	@Override
	public boolean isExporter() {
		return false;
	}

	@Override
	public boolean isImporter() {
		return true;
	}

	private String method = "?";
	private boolean check = Final.D_CHECK;
	private int timeout = Final.D_TIMEOUT;
	private String loadbalance = Final.D_LOADBALANCE_VALUE;
	private boolean async = Final.D_ASYNC;
	private transient Object proxy;
	
	public boolean isCheck() {
		return check;
	}

	public Importer setCheck(boolean check) {
		this.check = check;
		return this;
	}

	public int getTimeout() {
		return timeout;
	}

	public Importer setTimeout(int timeout) {
		this.timeout = timeout;
		return this;
	}

	public String getLoadbalance() {
		return loadbalance;
	}

	public Importer setLoadbalance(String loadbalance) {
		this.loadbalance = loadbalance;
		return this;
	}

	public boolean isAsync() {
		return async;
	}

	public Importer setAsync(boolean async) {
		this.async = async;
		return this;
	}
	
	public String getMethod() {
		return method;
	}

	public Importer setMethod(String method) {
		this.method = method;
		return this;
	}

	@SuppressWarnings("unchecked")
	public <T> T getProxy(Class<T> type){
		return (T)proxy;
	}

	public Importer setProxy(Object proxy) {
		this.proxy = proxy;
		return this;
	}

}