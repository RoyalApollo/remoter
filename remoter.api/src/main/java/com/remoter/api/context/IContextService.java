package com.remoter.api.context;

import java.util.concurrent.ConcurrentHashMap;

import com.remoter.api.context.support.Exporter;
import com.remoter.api.context.support.Importer;
import com.remoter.api.exception.ProviderNotFoundException;
import com.remoter.api.extension.annotation.Extension;

@Extension
public interface IContextService {
	
	public void start();
	public void close();
	
	public void attachExporter(Exporter exporter);
	public void detachExporter(String beanName,Class<?> type);
	public Exporter getExporter(String beanName,Class<?> type);
	
	public void attachImporter(Importer importer);
	public void detachImporter(String beanName,Class<?> type);
	public Importer getImporter(String beanName,Class<?> type);
	
	public Exporter createExporter(String beanName,Class<?> type,Object instance);
	public Importer createImporter(String beanName,Class<?> type);
	
	public Exporter select(Importer importer) throws ProviderNotFoundException;
	
	public String getDefaultLoadBalance();
	
	public ConcurrentHashMap<String,Exporter> getExporters();
	public ConcurrentHashMap<String,Importer> getImporters();
	public ConcurrentHashMap<String,IModule> getServices();
	
}