package com.remoter.api.context.support;

import com.remoter.api.util.Final;


public final class Exporter extends AbstractModule{
	
	private static final long serialVersionUID = 8674615140967773764L;
	
	protected Exporter(String server, String host, int port, String bean,Class<?> type) {
		super(server, host, port, bean, type);
	}
	
	private int weight = Final.D_LOADBALANCE_WEIGHT;
	private int executes = Final.D_EXECUTES;
	private transient Object instance;
	
	public int getWeight() {
		return weight;
	}
	
	public Exporter setWeight(int weight) {
		this.weight = weight;
		return this;
	}

	public int getExecutes() {
		return executes;
	}

	public Exporter setExecutes(int executes) {
		this.executes = executes;
		return this;
	}
	
	public Object getInstance() {
		return instance;
	}

	public Exporter setInstance(Object instance) {
		this.instance = instance;
		return this;
	}
	
	@Override
	public boolean isExporter(){
		return true;
	}

	@Override
	public boolean isImporter() {
		return false;
	}
	
}