package com.remoter.api.context;

import java.io.Serializable;
import java.net.InetSocketAddress;

public interface IModule extends Serializable{
	
	public boolean isExporter();
	public boolean isImporter();
	
	public <T> T parseModule(Class<T> type);
	
	public IModule setServer(String server);
	public String getServer();
	
	public IModule setHost(String host);
	public String getHost();
	
	public IModule setPort(int port);
	public int getPort();
	
	public InetSocketAddress getSocketAddress();
	
	public IModule setType(Class<?> type);
	public Class<?> getType();
	
	public IModule setBean(String bean);
	public String getBean();
	
	public String getPath();
	public String getServiceKey();
	
	public long getStartTime();
	
}