package com.remoter.api.configure;

import com.remoter.api.extension.annotation.Extension;

@Extension
public interface IConfiguration {
	
	public <T> T getOption(Option<T> option);
	
}