package com.remoter.api.extension.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 具体扩展点实例对应名称,可在配置文件中配置或使用当前注解设置,如果同时设置,已注解设置的为主
 * @author koko
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExtensionName {
	
	public String value();
	
}