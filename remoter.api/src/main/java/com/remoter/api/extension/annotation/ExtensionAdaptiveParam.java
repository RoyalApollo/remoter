package com.remoter.api.extension.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 参数判定标志,如果同一个方法中存在多个注解,则一次按照参数顺序进行查找,使用第一个找到的结果
 * @author koko
 *
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExtensionAdaptiveParam {
	
}