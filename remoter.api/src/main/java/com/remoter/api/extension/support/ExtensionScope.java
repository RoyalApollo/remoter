package com.remoter.api.extension.support;

/**
 * 扩展点实例作用域
 * @author koko
 *
 */
public enum ExtensionScope {
	
	Prototype,//每次创建新的扩展点实例
	Singleton//单例模式
	
}