package com.remoter.api.extension.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.remoter.api.extension.support.ExtensionScope;

/**
 * 扩展点注解,只能标注在interface上
 * @author koko
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Extension {
	
	public String value() default "";//是否有默认扩展点,默认为空
	public ExtensionScope scope() default ExtensionScope.Singleton;//模式扩展点实例方式,默认为单例
	
}