package com.remoter.api.extension.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 动态适配注解,标记当前方法获取的扩展点为动态适配的,必须配置@ExtensionAdaptiveParam扩展点使用,
 * 通常使用在扩展点Factory上
 * @author koko
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExtensionAdaptive {
	
}