package com.remoter.api.transport;

import java.net.InetSocketAddress;

import com.remoter.api.exception.ClientUnAvailableException;
import com.remoter.api.exception.ConnectErrorException;
import com.remoter.api.exception.ReadTimeoutException;
import com.remoter.api.exception.WriteTimeoutException;
import com.remoter.api.extension.annotation.Extension;
import com.remoter.api.extension.support.ExtensionScope;
import com.remoter.api.protocol.support.RemoterRequest;
import com.remoter.api.protocol.support.RemoterResponse;
import com.remoter.api.util.INode;

@Extension(scope=ExtensionScope.Prototype)
public interface IClient extends INode{
	
	public void connect(InetSocketAddress remote)throws ConnectErrorException;
	
	public RemoterResponse sendRemoterRequest(RemoterRequest remoterRequest)throws ClientUnAvailableException,ReadTimeoutException,WriteTimeoutException;
	
	public InetSocketAddress getLocalAddress();
	
}