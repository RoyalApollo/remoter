package com.remoter.api.transport.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.remoter.api.transport.IClient;

public abstract class AbstractClient extends AbstractNode implements IClient{
	
	protected static final Logger logger = LoggerFactory.getLogger(AbstractClient.class);
	
}