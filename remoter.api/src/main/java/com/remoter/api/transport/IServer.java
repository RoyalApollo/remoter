package com.remoter.api.transport;

import java.net.BindException;
import java.net.InetSocketAddress;

import com.remoter.api.extension.annotation.Extension;
import com.remoter.api.extension.support.ExtensionScope;
import com.remoter.api.util.INode;

@Extension(scope=ExtensionScope.Prototype)
public interface IServer extends INode{
	
	public void bind(InetSocketAddress bind)throws InterruptedException,BindException;
	
}