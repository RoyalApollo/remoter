package com.remoter.api.transport.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.remoter.api.transport.IServer;

public abstract class AbstractServer extends AbstractNode implements IServer{

	protected static final Logger logger = LoggerFactory.getLogger(AbstractServer.class);
	
}