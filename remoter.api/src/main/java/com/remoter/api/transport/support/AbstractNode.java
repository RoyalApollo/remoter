package com.remoter.api.transport.support;

import com.remoter.api.configure.IConfiguration;
import com.remoter.api.configure.support.AbstractConfiguration;
import com.remoter.api.extension.support.ExtensionLoader;
import com.remoter.api.serialize.ISerialization;
import com.remoter.api.util.Final;
import com.remoter.api.util.INode;

public abstract class AbstractNode implements INode{
	
	protected final IConfiguration configuration;
	protected final ISerialization serialization;
	public AbstractNode(){
		this.configuration = AbstractConfiguration.getConfiguration();
		this.serialization = ExtensionLoader.getService(ISerialization.class,this.configuration.getOption(Final.O_SERVER_SERIALIZE));
	}
	
}