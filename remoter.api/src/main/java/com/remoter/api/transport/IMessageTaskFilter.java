package com.remoter.api.transport;

import com.remoter.api.extension.annotation.Extension;
import com.remoter.api.extension.support.ExtensionScope;
import com.remoter.api.protocol.support.RemoterRequest;
import com.remoter.api.protocol.support.RemoterResponse;
import com.remoter.api.util.ObjectChain;

/**
 * @author			koko
 * @date 			2017-06-29 18:47:43
 * @description 	类功能说明
 */
@Extension(scope=ExtensionScope.Prototype)
public interface IMessageTaskFilter {
	
	public void execute(RemoterRequest remoterRequest,RemoterResponse remoterResponse,ObjectChain<IMessageTaskFilter> chain);
	
}