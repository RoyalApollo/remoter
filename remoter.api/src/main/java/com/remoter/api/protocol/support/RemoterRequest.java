package com.remoter.api.protocol.support;
import com.remoter.api.protocol.IMessage;
import com.remoter.api.util.Guid;

public class RemoterRequest implements IMessage{

	private static final long serialVersionUID = 1L;
	
	private long token;
	private String serviceClass;
	private String serviceBeanName;
	private String serviceMethodName;
	private Class<?>[] serviceParamTypes;
	private Object[] serviceParamObjects;
	
	public RemoterRequest(){
		this.reset();
	}
	
	public void reset(){
		this.token = Guid.get();
		this.serviceClass = null;
		this.serviceBeanName = null;
		this.serviceMethodName = null;
		this.serviceParamTypes = null;
		this.serviceParamObjects = null;
	}
	
	public long getToken() {
		return token;
	}
	public String getServiceClass() {
		return serviceClass;
	}
	public void setServiceClass(String serviceClass) {
		this.serviceClass = serviceClass;
	}
	public String getServiceMethodName() {
		return serviceMethodName;
	}
	public void setServiceMethodName(String serviceMethodName) {
		this.serviceMethodName = serviceMethodName;
	}
	public Class<?>[] getServiceParamTypes() {
		return serviceParamTypes;
	}
	public void setServiceParamTypes(Class<?>[] serviceParamTypes) {
		this.serviceParamTypes = serviceParamTypes;
	}
	public Object[] getServiceParamObjects() {
		return serviceParamObjects;
	}
	public void setServiceParamObjects(Object[] serviceParamObjects) {
		this.serviceParamObjects = serviceParamObjects;
	}
	public String getServiceBeanName() {
		return serviceBeanName;
	}
	public void setServiceBeanName(String serviceBeanName) {
		this.serviceBeanName = serviceBeanName;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder(super.toString());
		sb.append("{");
		sb.append("token:").append(this.token).append(",");
		sb.append("serviceClass:").append(this.serviceClass).append(",");
		sb.append("serviceBeanName:").append(this.serviceBeanName).append(",");
		sb.append("serviceMethodName:").append(this.serviceMethodName).append(",");
		sb.append("serviceParamTypes:").append(this.serviceParamTypes).append(",");
		sb.append("serviceParamObjects:").append(this.serviceParamObjects).append("}");
		return sb.toString();
	}
	
}
