package com.remoter.api.protocol.support;
import com.remoter.api.protocol.IMessage;
import com.remoter.api.util.StringUtil;

public class RemoterResponse implements IMessage{

	private static final long serialVersionUID = 1L;
	
	private final long token;
	private Object data;
	private String exception;
	
	public RemoterResponse(long token){
		this.token = token;
	}
	
	public long getToken() {
		return token;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getException() {
		return exception;
	}
	public void setException(String exception) {
		this.exception = exception;
	}
	public boolean hasException(){
		return StringUtil.isBlank(this.exception) ? false : true;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder(super.toString());
		sb.append("{");
		sb.append("token:").append(this.token).append(",");
		sb.append("data:").append(this.data).append(",");
		sb.append("exception:").append(this.exception).append("}");
		return sb.toString();
	}
	
}