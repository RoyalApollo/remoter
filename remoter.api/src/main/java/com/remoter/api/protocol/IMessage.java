package com.remoter.api.protocol;

import java.io.Serializable;

public interface IMessage extends Serializable{
	
	public static final int MESSAGE_LENGTH = 4;
	
}