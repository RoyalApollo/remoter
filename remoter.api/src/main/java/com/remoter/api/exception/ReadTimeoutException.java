package com.remoter.api.exception;

import java.util.concurrent.TimeoutException;

public class ReadTimeoutException extends TimeoutException{

	private static final long serialVersionUID = -3656705611438790617L;
	
	public static final ReadTimeoutException INSTANCE = new ReadTimeoutException();
	
	private ReadTimeoutException(){}
	
}