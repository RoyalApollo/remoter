package com.remoter.api.exception;

public class RemoterException extends Exception{

	private static final long serialVersionUID = -2788036811373517425L;

	public RemoterException() {
		super();
	}

	public RemoterException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public RemoterException(String message, Throwable cause) {
		super(message, cause);
	}

	public RemoterException(String message) {
		super(message);
	}

	public RemoterException(Throwable cause) {
		super(cause);
	}

}