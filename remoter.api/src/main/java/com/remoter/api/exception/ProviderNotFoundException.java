package com.remoter.api.exception;

public class ProviderNotFoundException extends Exception{

	private static final long serialVersionUID = -5995566525249970358L;
	
	private ProviderNotFoundException(){}
	
	public static final ProviderNotFoundException INSTANCE = new ProviderNotFoundException();
	
}