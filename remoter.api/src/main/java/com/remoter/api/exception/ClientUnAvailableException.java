package com.remoter.api.exception;

public class ClientUnAvailableException extends Exception{
	
	private static final long serialVersionUID = 4827178334896549425L;
	
	public static final ClientUnAvailableException INSTANCE = new ClientUnAvailableException();
	
	private ClientUnAvailableException(){}
	
}