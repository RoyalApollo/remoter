package com.remoter.api.exception;

public class ConnectErrorException extends Exception{

	private static final long serialVersionUID = 816629506470627483L;

	public static final ConnectErrorException INSTANCE = new ConnectErrorException();

	public ConnectErrorException() {
		super();
	}

	public ConnectErrorException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ConnectErrorException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConnectErrorException(String message) {
		super(message);
	}

	public ConnectErrorException(Throwable cause) {
		super(cause);
	}
	
}