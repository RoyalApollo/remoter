package com.remoter.api.exception;

import java.util.concurrent.TimeoutException;

public class WriteTimeoutException extends TimeoutException{

	private static final long serialVersionUID = -3656705611438790617L;
	
	public static final WriteTimeoutException INSTANCE = new WriteTimeoutException();
	
	private WriteTimeoutException(){}
	
}