package com.remoter.api.provider;

import java.net.BindException;
import java.net.InetSocketAddress;

import com.remoter.api.extension.annotation.Extension;
import com.remoter.api.transport.IMessageTask;
import com.remoter.api.transport.IMessageTaskCallBack;

@Extension
public interface IProviderService {
	
	public void submit(IMessageTask messageTask,IMessageTaskCallBack messageTaskCallBack);
	
	public void bind(InetSocketAddress bind) throws BindException, InterruptedException;
	
	public void unBind();
	
	public InetSocketAddress getBindSocketAddress();
	
}