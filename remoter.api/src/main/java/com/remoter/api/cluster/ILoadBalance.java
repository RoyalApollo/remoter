package com.remoter.api.cluster;

import java.util.List;

import com.remoter.api.context.support.Exporter;
import com.remoter.api.context.support.Importer;
import com.remoter.api.extension.annotation.Extension;

@Extension
public interface ILoadBalance {
	
	public Exporter select(Importer importer,List<Exporter> exporters);
	
}