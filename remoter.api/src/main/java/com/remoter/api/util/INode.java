package com.remoter.api.util;

public interface INode {
	
	public void disConnect();
	
	public boolean isAvailable();
	
}