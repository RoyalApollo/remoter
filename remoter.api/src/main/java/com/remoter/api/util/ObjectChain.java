package com.remoter.api.util;

import java.util.ArrayDeque;
import java.util.Iterator;

public class ObjectChain<E> extends ArrayDeque<E> {

	private static final long serialVersionUID = 1L;
	
	private Iterator<E> iterator;
	private Iterator<E> inverted;
	private boolean stop;
	private E current;
	
	public ObjectChain(){
		this.stop = false;
		this.rewind();
	}
	
	public void rewind(){
		this.stop = false;
		this.iterator = this.iterator();
		this.inverted = this.descendingIterator();
	}
	
	public void stop(){
		this.stop = true;
	}
	
	public E current(){
		return this.current;
	}
	
	public E next(){
		if(this.stop){
			return null;
		}else{
			if(this.iterator.hasNext()){
				this.current = this.iterator.next();
				return this.current;
			}else{
				return null;
			}
		}
	}
	
	public E prev(){
		if(this.stop){
			return null;
		}else{
			if(this.inverted.hasNext()){
				this.current = inverted.next();
				return this.current;
			}else{
				return null;
			}
		}
	}
	
	public boolean hasNext(){
		if(this.stop){
			return false;
		}else{
			return this.iterator.hasNext();
		}
	}
	
	public boolean hasPrev(){
		if(this.stop){
			return false;
		}else{
			return this.inverted.hasNext();
		}
	}
	
}