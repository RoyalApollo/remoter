package com.remoter.api.util;

public class StringUtil {
	
	public static boolean isBlank(String value){
		if(null == value || value.trim().length() == 0){
			return true;
		}else{
			return false;
		}
	}
	
	public static boolean isNotBlank(String value){
		return !isBlank(value);
	}
	
	public static boolean isEquals(String v1,String v2){
		if(null == v1 && null != v2){
			return false;
		}
		if(null != v1 && null == v2){
			return false;
		}
		return v1.equals(v2);
	}
	
	public static String join(String[] array){
		if( array.length == 0 ) return "";
		StringBuilder sb = new StringBuilder();
		for( String s : array )
			sb.append(s);
		return sb.toString();
	}
	
}