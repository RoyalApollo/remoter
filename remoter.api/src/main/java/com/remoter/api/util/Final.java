package com.remoter.api.util;

import com.remoter.api.configure.Option;

public class Final {
	
	public static final String D_DATA_SEPARATOR = ",";
	public static final int PROCRESS = Math.max(2,Runtime.getRuntime().availableProcessors());
	
	public static final String K_CONFIGURATION_NAME = "REMOTER_CONFIGURATION_NAME";
	public static final String K_CONFIGURATION_PATH = "REMOTER_CONFIGURATION_PATH";
	
	public static final String D_CONFIGURATION_NAME = "remoter";
	public static final String D_CONFIGURATION_PATH = "properties";
	
	public static final int D_LOADBALANCE_WEIGHT = 100;
	public static final int D_EXECUTES = 0;
	
	public static final int D_TIMEOUT = 3000;
	public static final String D_LOADBALANCE_VALUE = "random";
	public static final boolean D_ASYNC = false;
	public static final boolean D_CHECK = false;
	
	public static final Option<String> O_SERVER_NAME = Option.create("server.name","remoter");
	public static final Option<String> O_SERVER_SERIALIZE = Option.create("server.serialize","kryo");
	public static final Option<String> O_SERVER_PROVIDER = Option.create("server.provider","internal");
	public static final Option<String> O_SERVER_CONSUMER = Option.create("server.consumer","internal");
	public static final Option<String> O_SERVER_REGISTRY = Option.create("server.registry","zookeeper");
	public static final Option<String> O_SERVER_TRANSPORT = Option.create("server.transport","netty");
	public static final Option<String> O_SERVER_CONTEXT = Option.create("server.context","internal");
	public static final Option<String> O_SERVER_MONITOR = Option.create("server.monitor","");
	public static final Option<String> O_SERVER_CONTAINER = Option.create("server.container","");
	
}