package com.remoter.example.provider.impl;

import java.math.BigDecimal;

import com.remoter.context.spring.annotation.Provider;
import com.remoter.example.provider.IMathService;

@Provider(type=IMathService.class,name="bigDecimalMathService")
public class BigDecimalMathService implements IMathService{

	@Override
	public int add(int a,int b){
		BigDecimal _a = new BigDecimal(String.valueOf(a));
		BigDecimal _b = new BigDecimal(String.valueOf(b));
		return _a.add(_b).intValue();
	}
	
}