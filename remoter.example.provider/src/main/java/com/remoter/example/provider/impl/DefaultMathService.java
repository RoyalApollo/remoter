package com.remoter.example.provider.impl;

import com.remoter.context.spring.annotation.Provider;
import com.remoter.example.provider.IMathService;

@Provider(type=IMathService.class,name="defaultMathService")
public class DefaultMathService implements IMathService{

	@Override
	public int add(int a,int b) {
		return a + b;
	}
	
}