@echo off & setlocal enabledelayedexpansion

set CURRENT_DIR=%cd%
set LIBRARY_JAR=""

if exist "%CURRENT_DIR%\bin\server.bat" goto OK_HOME

cd %CURRENT_DIR%

if exist "%CURRENT_DIR%\server.bat" goto IN_HOME

echo the environment path is error
goto END

:OK_HOME

echo %cd%

cd .\lib
for %%i in (*) do set LIBRARY_JAR=!LIBRARY_JAR!;.\lib\%%i
cd ..\

java -Xms64m -Xmx1024m -Dcom.sun.management.jmxremote.port=1099 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -classpath .\conf;%LIBRARY_JAR% remoter.core.BootStrap remoter.properties

:IN_HOME
cd ..
set CURRENT_DIR=%cd%
goto OK_HOME

:END