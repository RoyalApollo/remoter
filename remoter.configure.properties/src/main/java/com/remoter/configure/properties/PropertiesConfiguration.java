package com.remoter.configure.properties;

import java.io.InputStream;
import java.util.Map.Entry;
import java.util.Properties;

import com.remoter.api.configure.support.AbstractConfiguration;
import com.remoter.api.extension.annotation.ExtensionName;

@ExtensionName("properties")
public class PropertiesConfiguration extends AbstractConfiguration{
	
	private Properties properties = new Properties();
	
	public PropertiesConfiguration(){
		String path = this.getConfig();
		try(InputStream inputStream = this.getClass().getResourceAsStream(path)){
			if(null == inputStream){
				logger.warn("config file not found :" + path);
			}else{
				this.properties.load(inputStream);
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}
		if(logger.isDebugEnabled()){
			for(Entry<Object,Object> entry : this.properties.entrySet()){
				logger.debug(String.format("load config [%s=%s]",entry.getKey(),entry.getValue()));
			}
		}
	}
	
	@Override
	protected String getValue(String name) {
		return this.properties.getProperty(name);
	}
	
}