模仿dubbo设计思路开发出来的一套分布式服务框架,本地测试通过,未经过生产环境测试,所以建议用户经过严格测试或升级后再考虑生产环境使用.
发布出来是希望能给做这方面研究的人员参考,如果有问题还希望各位酌情拍砖.

框架基于netty4实现socket通讯
基于apache curator实现zookeeper服务中心的注册与发现,
参考dubbo的spi机制进行部分扩展与重构
剔除了dubbo的Url思路
无缝集成了Spring框架
使用kryo实现序列化功能
负载均衡集成进了dubbo内置的几种算法

注:
以上功能仅适合研究不学习,暂不适合生产环境,慎重.
monitor目前开发了部分,还没有开发结束

配置项:
	每个项目中的util包中存在一个Final开头的类,此类中的Option类型为系统配置项及默认值
测试步骤:
	1.启动zookeeper
	2.修改配置文件remoter.example项目中的src/test/resources/remoter.properties文件中的[registry.zookeeper.connection_address]为zookeeper的连接地址
	3.启动com.remoter.example.spring.TestSpringProvider类中的main函数
	4.启动com.remoter.example.spring.TestSpringConsumer类中的main函数(启动后会报一个端口占用异常,忽略即可,因为同一台服务启动了多个provider,配置文件中并未修改provider的端口导致的)

如有希望参与开发的人员可留言或联系QQ:191817533
谢谢.