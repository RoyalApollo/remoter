package com.remoter.example.consumer.impl;

import org.springframework.stereotype.Service;

import com.remoter.context.spring.annotation.Consumer;
import com.remoter.example.consumer.IService;
import com.remoter.example.provider.IMathService;

@Service
public class ServiceImpl implements IService{
	
	@Consumer(name="defaultMathService")
	private IMathService mathService;

	@Override
	public String getValue(String name) {
		System.out.println(this.mathService);
		return name + " : " + String.valueOf(this.mathService.add(10,1000));
	}
	
}