package com.remoter.example.consumer;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestService {
	
	@Autowired
	private IService service;
	
	@PostConstruct
	public void run(){
		System.out.println(this.service);
	}
	
}