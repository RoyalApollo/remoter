package com.remoter.example.consumer;

public interface IService {
	
	public String getValue(String name);
	
}