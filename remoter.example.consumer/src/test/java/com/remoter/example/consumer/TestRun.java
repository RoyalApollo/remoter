package com.remoter.example.consumer;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;


public class TestRun {
	
	public static void main(String[] args) throws IOException {
		try(ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-core.xml")){
			while(true){
				try{
					System.out.println(context.getBean(IService.class).getValue("测试"));
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
	
}