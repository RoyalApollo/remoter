package com.remoter.transport.netty.handler;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.net.InetSocketAddress;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.util.concurrent.SettableFuture;
import com.remoter.api.consumer.IConsumerService;
import com.remoter.api.exception.WriteTimeoutException;
import com.remoter.api.protocol.support.RemoterRequest;
import com.remoter.api.protocol.support.RemoterResponse;

public class ClientMessageHandler extends ChannelInboundHandlerAdapter{
	
	private ConcurrentHashMap<Long,SettableFuture<RemoterResponse>> callbacks = new ConcurrentHashMap<Long,SettableFuture<RemoterResponse>>();
	private IConsumerService consumerService;
	private volatile Channel channel;
	private volatile InetSocketAddress address;
	
	public ClientMessageHandler(IConsumerService consumerService){
		this.consumerService = consumerService;
	}
	
	@Override
	public void channelRegistered(ChannelHandlerContext ctx) throws Exception{
		this.channel = ctx.channel();
	}
	
	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		this.address = (InetSocketAddress)this.channel.remoteAddress();
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)throws Exception{
		if(null != msg && msg instanceof RemoterResponse){
			RemoterResponse remoterResponse = (RemoterResponse)msg;
			long token = remoterResponse.getToken();
			if(this.callbacks.containsKey(token)){
				SettableFuture<RemoterResponse> future = this.callbacks.get(token);
				this.callbacks.remove(token);
				future.set(remoterResponse);
			}
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx,Throwable cause)throws Exception{
		this.consumerService.disConnect(this.address);
		this.close();
		ctx.close();
	}
	
	public SettableFuture<RemoterResponse> sendRemoterRequest(int timeout,RemoterRequest remoterRequest)throws WriteTimeoutException{
		SettableFuture<RemoterResponse> callback = SettableFuture.create();
		this.callbacks.put(remoterRequest.getToken(),callback);
		try{
			this.channel.writeAndFlush(remoterRequest);
		}catch(Exception e){
			throw WriteTimeoutException.INSTANCE;
		}
		return callback;
	}
	
	public void close(){
		for(Entry<Long,SettableFuture<RemoterResponse>> entry : this.callbacks.entrySet()){
			entry.getValue().cancel(true);
		}
		this.callbacks.clear();
		this.channel.close();
	}
	
	public boolean isAvailable(){
		return this.channel.isOpen();
	}
	
}