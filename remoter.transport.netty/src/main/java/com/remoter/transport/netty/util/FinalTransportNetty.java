package com.remoter.transport.netty.util;

import com.remoter.api.configure.Option;

public class FinalTransportNetty {
	
	public static final Option<Integer> O_TRANSPORT_CLIENT_CONNECT_TIMEOUT = Option.create("transport.client.connect_timeout",12000);
	public static final Option<Integer> O_TRANSPORT_CLIENT_WRITE_TIMEOUT = Option.create("transport.client.write_timeout",3000);
	public static final Option<Integer> O_TRANSPORT_CLIENT_READ_TIMEOUT = Option.create("transport.client.read_timeout",3000);
	public static final Option<String> O_TRANSPORT_SERVER_FILTER = Option.create("transport.server.filter","executes,rpc");
	
}