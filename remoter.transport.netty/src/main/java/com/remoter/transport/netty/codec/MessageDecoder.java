package com.remoter.transport.netty.codec;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.remoter.api.protocol.IMessage;
import com.remoter.api.serialize.ISerialization;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

public class MessageDecoder extends ByteToMessageDecoder{
	
	private static final Logger log = LoggerFactory.getLogger(MessageDecoder.class);
	private ISerialization serialization;
	
	public MessageDecoder(ISerialization serialization){
		this.serialization = serialization;
	}
	
	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		if(in.readableBytes() < IMessage.MESSAGE_LENGTH){
			return;
		}
		in.markReaderIndex();
		int length = in.readInt();
		if(length < 0){
			ctx.close();
		}
		if(in.readableBytes() < length){
			in.resetReaderIndex();
			return;
		}else{
			byte[] content = new byte[length];
			in.readBytes(content);
			try(ByteArrayInputStream bais = new ByteArrayInputStream(content)){
				Object obj = this.serialization.deserialize(bais);
				out.add(obj);
			}catch(Exception e){
				log.error(e.getMessage(),e);
			}
		}
	}
	
}