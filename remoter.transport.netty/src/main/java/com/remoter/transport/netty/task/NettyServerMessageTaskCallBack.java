package com.remoter.transport.netty.task;

import io.netty.channel.ChannelHandlerContext;

import com.remoter.api.protocol.support.RemoterRequest;
import com.remoter.api.protocol.support.RemoterResponse;
import com.remoter.api.transport.IMessageTaskCallBack;

public class NettyServerMessageTaskCallBack implements IMessageTaskCallBack{
	
	private final RemoterResponse remoterResponse;
	private final ChannelHandlerContext channelHandlerContext;
	
	public NettyServerMessageTaskCallBack(RemoterRequest remoterRequest,RemoterResponse remoterResponse,ChannelHandlerContext ctx){
		this.remoterResponse = remoterResponse;
		this.channelHandlerContext = ctx;
	}
	
	@Override
	public void onFailure(Throwable throwable) {
		this.remoterResponse.setException(throwable.getMessage());
		this.channelHandlerContext.writeAndFlush(this.remoterResponse);
	}
	
	@Override
	public void onSuccess(Boolean value) {
		this.channelHandlerContext.writeAndFlush(this.remoterResponse);
	}
	
}