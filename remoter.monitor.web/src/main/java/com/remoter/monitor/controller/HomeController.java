package com.remoter.monitor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.remoter.monitor.service.IManagementService;

@Controller
public class HomeController {
	
	@Autowired
	private IManagementService managementService;
	
	@RequestMapping(value={"/","/index.html"})
	public String index(){
		return "home/index";
	}
	
	@PostMapping("/host.json")
	@ResponseBody
	public Object hosts(){
		return this.managementService.hosts();
	}
	
}