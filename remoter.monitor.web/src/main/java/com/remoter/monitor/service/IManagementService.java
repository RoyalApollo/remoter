package com.remoter.monitor.service;

import java.util.List;

public interface IManagementService {
	
	public void start();
	
	public void close();
	
	public List<String> hosts();
	
}