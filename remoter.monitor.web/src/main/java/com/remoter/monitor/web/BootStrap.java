package com.remoter.monitor.web;

import io.undertow.servlet.api.ListenerInfo;
import io.undertow.servlet.api.ServletInfo;

import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.servlet.DispatcherServlet;

import com.remoter.monitor.util.Application;

public class BootStrap extends Application{

	public BootStrap(Class<?> type){
		super(type);
	}

	@Override
	public void initDeploymentInfo() {
		ListenerInfo spring = new ListenerInfo(ContextLoaderListener.class);
		
		ServletInfo springmvc = new ServletInfo("mvc",DispatcherServlet.class);
		springmvc.setLoadOnStartup(1);
		springmvc.addInitParam("contextConfigLocation","classpath:spring-mvc.xml");
		springmvc.addMapping("/");
		
		this.deploymentInfo.addInitParameter("contextConfigLocation","classpath:spring-core.xml");
		this.deploymentInfo.addListener(spring);
		this.deploymentInfo.addServlet(springmvc);
	}

}