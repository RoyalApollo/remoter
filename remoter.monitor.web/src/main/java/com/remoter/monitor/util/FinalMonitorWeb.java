package com.remoter.monitor.util;

import com.remoter.api.configure.Option;
import com.remoter.api.util.NetUtil;

public class FinalMonitorWeb {
	
	public static final Option<String> O_MONITOR_BOOTSTRAP = Option.create("monitor.bootstrap","");
	public static final Option<String> O_MONITOR_HOST = Option.create("monitor.host",NetUtil.getLocalAddress().getHostAddress());
	public static final Option<Integer> O_MONITOR_PORT = Option.create("monitor.port",9527);
	public static final Option<String> O_MONITOR_CONTEXT = Option.create("monitor.context","/monitor");
	public static final Option<String> O_MONITOR_WELCOME = Option.create("monitor.welcome","/index.html");
	
}