package com.remoter.consumer.internal.util;

import com.remoter.api.configure.Option;
import com.remoter.api.util.NetUtil;

public class FinalConsumerInternal {
	
	public static final Option<String> O_CONSUMER_INTERNAL_HOST = Option.create("consumer.internal.host",NetUtil.getLocalAddress().getHostAddress());
	public static final Option<Integer> O_CONSUMER_INTERNAL_PORT = Option.create("consumer.internal.port",0);
	
}