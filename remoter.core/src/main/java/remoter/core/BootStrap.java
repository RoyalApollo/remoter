package remoter.core;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.remoter.api.configure.IConfiguration;
import com.remoter.api.configure.support.AbstractConfiguration;
import com.remoter.api.container.IContainer;
import com.remoter.api.extension.support.ExtensionLoader;
import com.remoter.api.util.Final;
import com.remoter.api.util.StringUtil;

public class BootStrap {
	
	private static final Logger logger = LoggerFactory.getLogger(BootStrap.class);
	private static volatile boolean running = true;
	
	public static void main(String[] args){
		initConfiguration(args);
		IConfiguration configuration = AbstractConfiguration.getConfiguration();
		String containerString = configuration.getOption(Final.O_SERVER_CONTAINER);
		if(StringUtil.isBlank(containerString)){
			throw new IllegalArgumentException("container is empty");
		}
		String[] containerNames = containerString.split(Final.D_DATA_SEPARATOR);
		if(null == containerNames || containerNames.length == 0){
			throw new IllegalArgumentException("container not found");
		}
		final List<IContainer> containers = new ArrayList<IContainer>();
		for(String containerName : containerNames){
			if(StringUtil.isBlank(containerName)){
				logger.warn("container name is empty");
				continue;
			}
			IContainer container = ExtensionLoader.getService(IContainer.class,containerName);
			if(null == container){
				logger.warn("container is not found");
				continue;
			}
			logger.debug("found container for name["+containerName+"]");
			containers.add(container);
		}
		if(containers.isEmpty()){
			throw new IllegalArgumentException("container is empty");
		}
		Runtime.getRuntime().addShutdownHook(new Thread(){
			@Override
			public void run(){
				for(IContainer container : containers){
					try{
						container.close();
					}catch(Exception e){
						logger.error(e.getMessage(),e);
					}
					synchronized(BootStrap.class){
						running = false;
						BootStrap.class.notify();
					}
				}
			}
		});
		try{
			for(IContainer c : containers){
				c.start();
			}
			logger.info("Remoter Server Started");
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			System.exit(1);
		}
		synchronized(BootStrap.class){
			while(running){
				try{
					BootStrap.class.wait();
				}catch(Exception e){}
			}
		}
	}
	
	protected static void initConfiguration(String[] args){
		String name = Final.D_CONFIGURATION_NAME;
		String path = Final.D_CONFIGURATION_PATH;
		if(null != args){
			if(args.length == 1){
				String temp = args[0];
				if(temp.startsWith(".")){
					path = temp.substring(1);
				}else if(temp.endsWith(".")){
					name = temp.substring(0,temp.length()-1);
				}else{
					if(temp.indexOf(".")>0){
						String[] config = temp.split("\\.");
						name = config[0];
						path = config[1];
					}else{
						path = temp;
					}
				}
			}else if(args.length == 2){
				name = args[0];
				path = args[1];
			}
		}
		System.setProperty(Final.K_CONFIGURATION_NAME,name);
		System.setProperty(Final.K_CONFIGURATION_PATH,path);
		logger.info(String.format("set property %s=%s",Final.K_CONFIGURATION_NAME,name));
		logger.info(String.format("set property %s=%s",Final.K_CONFIGURATION_PATH,path));
	}
	
}