package com.remoter.registry.zookeeper.util;

import com.remoter.api.configure.Option;

public class FinalRegistryZookeeper {
	
	public static Option<Integer> O_REGISTRY_CONNECTION_TIMEOUT = Option.create("registry.zookeeper.connection_timeout",30000);
	public static Option<String> O_REGISTRY_CONNECTION_ADDRESS = Option.create("registry.zookeeper.connection_address","localhost:2181");
	public static Option<String> O_REGISTRY_NAMESPACE = Option.create("registry.zookeeper.namespace","remoter");
	public static Option<Integer> O_REGISTRY_SESSION_TIMEOUT = Option.create("registry.zookeeper.session_timeout",30000);
	public static Option<Integer> O_REGISTRY_RETRY_TIME_SLEEP = Option.create("registry.zookeeper.retry_time_sleep",1000);
	public static Option<Integer> O_REGISTRY_RETRY_TIME_COUNT = Option.create("registry.zookeeper.retry_time_count",5);
	
}