package com.remoter.serialize.kryo;

import java.io.IOException;

import com.remoter.api.extension.support.ExtensionLoader;
import com.remoter.api.protocol.support.RemoterRequest;
import com.remoter.api.serialize.ISerialization;

public class TestKryo {
	
	public static void main(String[] args) throws IOException {
		
		ISerialization serialization = ExtensionLoader.getService(ISerialization.class,"kryo");
		serialization.serialize(new RemoterRequest(),System.out);
		
	}
	
}