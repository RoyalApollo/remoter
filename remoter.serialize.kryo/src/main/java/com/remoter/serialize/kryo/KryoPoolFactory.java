package com.remoter.serialize.kryo;

import org.objenesis.strategy.StdInstantiatorStrategy;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.pool.KryoFactory;
import com.esotericsoftware.kryo.pool.KryoPool;
import com.remoter.api.protocol.support.RemoterRequest;
import com.remoter.api.protocol.support.RemoterResponse;

public class KryoPoolFactory {
	
	private static volatile KryoPoolFactory poolFactory = null;
	
	private KryoFactory factory = new KryoFactory(){
		@Override
		public Kryo create(){
			Kryo kryo = new Kryo();
			kryo.setReferences(false);
			kryo.register(RemoterRequest.class);
			kryo.register(RemoterResponse.class);
			kryo.setInstantiatorStrategy(new StdInstantiatorStrategy());
			return kryo;
		}
	};
	
	private KryoPool pool = new KryoPool.Builder(factory).build();
	private KryoPoolFactory(){}
	private KryoPool getPool(){
		return pool;
	}
	
	public static KryoPool getInstance(){
		if(null == poolFactory){
			synchronized(KryoPoolFactory.class){
				if(null == poolFactory){
					poolFactory = new KryoPoolFactory();
				}
			}
		}
		return poolFactory.getPool();
    }
	
}